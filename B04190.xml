<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="B04190">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>The King of France's letter to the Earl of Tyrconnel, found in a ship laden with arms for Ireland.</title>
    <author>Louis XIV, King of France, 1638-1715.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription B04190 of text R180083 in the <ref target="http;//estc.bl.uk">English Short Title Catalog</ref> (Wing L3127). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">B04190</idno>
    <idno type="STC">Wing L3127</idno>
    <idno type="STC">ESTC R180083</idno>
    <idno type="EEBO-CITATION">53299190</idno>
    <idno type="OCLC">ocm 53299190</idno>
    <idno type="VID">179933</idno>
    <idno type="PROQUESTGOID">2240884314</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. B04190)</note>
    <note>Transcribed from: (Early English Books Online ; image set 179933)</note>
    <note>Images scanned from microfilm: (Early English Books, 1641-1700 ; 2808:38)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>The King of France's letter to the Earl of Tyrconnel, found in a ship laden with arms for Ireland.</title>
      <author>Louis XIV, King of France, 1638-1715.</author>
      <author>France. Sovereign (1643-1715 : Louis XIV).</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed for T.P.,</publisher>
      <pubPlace>London, :</pubPlace>
      <date>1688.</date>
     </publicationStmt>
     <notesStmt>
      <note>Caption title.</note>
      <note>Reproduction of original in: Bodleian Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>James -- II, -- King of England, 1633-1701 -- Early works to 1800.</term>
     <term>France -- Foreign relations -- Great Britain -- Early works to 1800.</term>
     <term>Great Britain -- Foreign relations -- France -- Early works to 1800.</term>
     <term>Great Britain -- Politics and government -- 1660-1688 -- Sources.</term>
     <term>Ireland -- History -- 1660-1688 -- Sources.</term>
     <term>Broadsides -- England -- 17th century.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>The King of France's letter to the Earl of Tyrconnel, found in a ship laden with arms for Ireland.</ep:title>
    <ep:author>Louis XIV, King of France, </ep:author>
    <ep:publicationYear>1688</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>427</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2008-04</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2008-08</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2008-10</date><label>John Pas</label>
        Sampled and proofread
      </change>
   <change><date>2008-10</date><label>John Pas</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2009-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="B04190-t">
  <body xml:id="B04190-e0">
   <div type="letter" xml:id="B04190-e10">
    <pb facs="tcp:179933:1" rend="simple:additions" xml:id="B04190-001-a"/>
    <head xml:id="B04190-e20">
     <w lemma="the" pos="d" xml:id="B04190-001-a-0010">THE</w>
     <w lemma="king" pos="n1" xml:id="B04190-001-a-0020">King</w>
     <w lemma="of" pos="acp" xml:id="B04190-001-a-0030">of</w>
     <w lemma="France" pos="nng1" xml:id="B04190-001-a-0040">France's</w>
     <w lemma="letter" pos="n1" xml:id="B04190-001-a-0050">Letter</w>
     <w lemma="to" pos="acp" xml:id="B04190-001-a-0060">TO</w>
     <w lemma="the" pos="d" xml:id="B04190-001-a-0070">THE</w>
     <w lemma="earl" pos="n1" xml:id="B04190-001-a-0080">Earl</w>
     <w lemma="of" pos="acp" xml:id="B04190-001-a-0090">of</w>
     <w lemma="Tyrconnel" pos="nn1" xml:id="B04190-001-a-0100">Tyrconnel</w>
     <pc xml:id="B04190-001-a-0110">,</pc>
     <w lemma="find" pos="vvn" xml:id="B04190-001-a-0120">Found</w>
     <w lemma="in" pos="acp" xml:id="B04190-001-a-0130">in</w>
     <w lemma="a" pos="d" xml:id="B04190-001-a-0140">a</w>
     <w lemma="ship" pos="n1" xml:id="B04190-001-a-0150">Ship</w>
     <w lemma="lade" pos="vvn" xml:id="B04190-001-a-0160">Laden</w>
     <w lemma="with" pos="acp" xml:id="B04190-001-a-0170">with</w>
     <w lemma="arm" pos="n2" xml:id="B04190-001-a-0180">Arms</w>
     <w lemma="for" pos="acp" xml:id="B04190-001-a-0190">for</w>
     <hi xml:id="B04190-e30">
      <w lemma="Ireland" pos="nn1" xml:id="B04190-001-a-0200">Ireland</w>
      <pc unit="sentence" xml:id="B04190-001-a-0210">.</pc>
     </hi>
    </head>
    <opener xml:id="B04190-e40">
     <salute xml:id="B04190-e50">
      <w lemma="my" pos="po" xml:id="B04190-001-a-0220">MY</w>
      <w lemma="lord" pos="n1" xml:id="B04190-001-a-0230">LORD</w>
      <pc xml:id="B04190-001-a-0240">,</pc>
     </salute>
    </opener>
    <p xml:id="B04190-e60">
     <w lemma="as" pos="acp" xml:id="B04190-001-a-0250">AS</w>
     <w lemma="we" pos="pns" xml:id="B04190-001-a-0260">we</w>
     <w lemma="be" pos="vvb" xml:id="B04190-001-a-0270">are</w>
     <w lemma="full" pos="av-j" xml:id="B04190-001-a-0280">fully</w>
     <w lemma="inform" pos="vvn" reg="Informed" xml:id="B04190-001-a-0290">Inform'd</w>
     <w lemma="of" pos="acp" xml:id="B04190-001-a-0300">of</w>
     <w lemma="the" pos="d" xml:id="B04190-001-a-0310">the</w>
     <w lemma="deplorable" pos="j" xml:id="B04190-001-a-0320">deplorable</w>
     <w lemma="misfortune" pos="n2" xml:id="B04190-001-a-0330">Misfortunes</w>
     <w lemma="of" pos="acp" xml:id="B04190-001-a-0340">of</w>
     <w lemma="our" pos="po" xml:id="B04190-001-a-0350">our</w>
     <w lemma="royal" pos="j" xml:id="B04190-001-a-0360">Royal</w>
     <w lemma="brother" pos="n1" xml:id="B04190-001-a-0370">Brother</w>
     <pc xml:id="B04190-001-a-0380">,</pc>
     <pc join="right" xml:id="B04190-001-a-0390">(</pc>
     <w lemma="his" pos="po" xml:id="B04190-001-a-0400">His</w>
     <w lemma="majesty" pos="n1" xml:id="B04190-001-a-0410">Majesty</w>
     <w lemma="of" pos="acp" xml:id="B04190-001-a-0420">of</w>
     <hi xml:id="B04190-e70">
      <w lemma="great" pos="j" xml:id="B04190-001-a-0430">Great</w>
      <w lemma="Britain" pos="nn1" xml:id="B04190-001-a-0440">Britain</w>
     </hi>
     <pc xml:id="B04190-001-a-0450">)</pc>
     <w lemma="and" pos="cc" xml:id="B04190-001-a-0460">and</w>
     <w lemma="of" pos="acp" xml:id="B04190-001-a-0470">of</w>
     <w lemma="his" pos="po" xml:id="B04190-001-a-0480">His</w>
     <w lemma="intention" pos="n2" xml:id="B04190-001-a-0490">Intentions</w>
     <w lemma="to" pos="prt" xml:id="B04190-001-a-0500">to</w>
     <w lemma="honour" pos="vvi" reg="Honour" xml:id="B04190-001-a-0510">Honor</w>
     <w lemma="our" pos="po" xml:id="B04190-001-a-0520">Our</w>
     <w lemma="court" pos="n1" xml:id="B04190-001-a-0530">Court</w>
     <w lemma="with" pos="acp" xml:id="B04190-001-a-0540">with</w>
     <w lemma="his" pos="po" xml:id="B04190-001-a-0550">His</w>
     <w lemma="presence" pos="n1" xml:id="B04190-001-a-0560">Presence</w>
     <pc xml:id="B04190-001-a-0570">,</pc>
     <w lemma="till" pos="acp" xml:id="B04190-001-a-0580">till</w>
     <w lemma="we" pos="pns" xml:id="B04190-001-a-0590">we</w>
     <w lemma="can" pos="vmb" xml:id="B04190-001-a-0600">can</w>
     <w lemma="accommodate" pos="vvi" xml:id="B04190-001-a-0610">Accommodate</w>
     <w lemma="his" pos="po" xml:id="B04190-001-a-0620">His</w>
     <w lemma="return" pos="n1" xml:id="B04190-001-a-0630">Return</w>
     <w lemma="to" pos="acp" xml:id="B04190-001-a-0640">to</w>
     <w lemma="his" pos="po" xml:id="B04190-001-a-0650">His</w>
     <w lemma="ungrateful" pos="j" xml:id="B04190-001-a-0660">Ungrateful</w>
     <hi xml:id="B04190-e80">
      <w lemma="country" pos="n1" xml:id="B04190-001-a-0670">Country</w>
     </hi>
     <w lemma="and" pos="cc" xml:id="B04190-001-a-0680">and</w>
     <hi xml:id="B04190-e90">
      <w lemma="kingdom" pos="n1" xml:id="B04190-001-a-0690">Kingdom</w>
      <pc xml:id="B04190-001-a-0700">,</pc>
     </hi>
     <w lemma="suitable" pos="j" xml:id="B04190-001-a-0710">suitable</w>
     <w lemma="to" pos="acp" xml:id="B04190-001-a-0720">to</w>
     <w lemma="his" pos="po" xml:id="B04190-001-a-0730">His</w>
     <w lemma="grandeur" pos="n1" xml:id="B04190-001-a-0740">Grandeur</w>
     <w lemma="and" pos="cc" xml:id="B04190-001-a-0750">and</w>
     <w lemma="merit" pos="n1" xml:id="B04190-001-a-0760">Merit</w>
     <pc xml:id="B04190-001-a-0770">;</pc>
     <w lemma="so" pos="av" xml:id="B04190-001-a-0780">so</w>
     <w lemma="we" pos="pns" xml:id="B04190-001-a-0790">we</w>
     <w lemma="think" pos="vvd" xml:id="B04190-001-a-0800">thought</w>
     <w lemma="it" pos="pn" xml:id="B04190-001-a-0810">it</w>
     <w lemma="convenient" pos="j" xml:id="B04190-001-a-0820">convenient</w>
     <w lemma="to" pos="prt" xml:id="B04190-001-a-0830">to</w>
     <w lemma="send" pos="vvi" xml:id="B04190-001-a-0840">send</w>
     <w lemma="you" pos="pn" xml:id="B04190-001-a-0850">You</w>
     <w lemma="the" pos="d" xml:id="B04190-001-a-0860">the</w>
     <w lemma="most" pos="avs-d" xml:id="B04190-001-a-0870">most</w>
     <w lemma="proper" pos="j" xml:id="B04190-001-a-0880">proper</w>
     <w lemma="instruction" pos="n2" xml:id="B04190-001-a-0890">Instructions</w>
     <w lemma="we" pos="pns" reg="We" xml:id="B04190-001-a-0900">VVe</w>
     <pc xml:id="B04190-001-a-0910">,</pc>
     <w lemma="and" pos="cc" xml:id="B04190-001-a-0920">and</w>
     <w lemma="our" pos="po" xml:id="B04190-001-a-0930">Our</w>
     <w lemma="council" pos="n1" xml:id="B04190-001-a-0940">Council</w>
     <pc xml:id="B04190-001-a-0950">,</pc>
     <w lemma="can" pos="vmd" xml:id="B04190-001-a-0960">could</w>
     <w lemma="suggest" pos="vvb" xml:id="B04190-001-a-0970">Suggest</w>
     <pc xml:id="B04190-001-a-0980">,</pc>
     <w lemma="whereby" pos="crq" xml:id="B04190-001-a-0990">whereby</w>
     <w lemma="we" pos="pns" xml:id="B04190-001-a-1000">we</w>
     <w lemma="may" pos="vmd" xml:id="B04190-001-a-1010">might</w>
     <w lemma="render" pos="vvi" xml:id="B04190-001-a-1020">render</w>
     <w lemma="your" pos="po" xml:id="B04190-001-a-1030">your</w>
     <w lemma="endeavour" pos="n2" xml:id="B04190-001-a-1040">endeavours</w>
     <w lemma="as" pos="acp" xml:id="B04190-001-a-1050">as</w>
     <w lemma="serviceable" pos="j" xml:id="B04190-001-a-1060">Serviceable</w>
     <w lemma="and" pos="cc" xml:id="B04190-001-a-1070">and</w>
     <w lemma="material" pos="j" xml:id="B04190-001-a-1080">Material</w>
     <w lemma="as" pos="acp" xml:id="B04190-001-a-1090">as</w>
     <w lemma="may" pos="vmb" xml:id="B04190-001-a-1100">may</w>
     <w lemma="be" pos="vvi" xml:id="B04190-001-a-1110">be</w>
     <w lemma="for" pos="acp" xml:id="B04190-001-a-1120">for</w>
     <w lemma="your" pos="po" xml:id="B04190-001-a-1130">your</w>
     <w lemma="royal" pos="j" xml:id="B04190-001-a-1140">Royal</w>
     <w lemma="master" pos="ng1" xml:id="B04190-001-a-1150">Master's</w>
     <w lemma="interest" pos="n1" xml:id="B04190-001-a-1160">Interest</w>
     <pc unit="sentence" xml:id="B04190-001-a-1170">.</pc>
     <w lemma="the" pos="d" xml:id="B04190-001-a-1180">The</w>
     <hi xml:id="B04190-e100">
      <w lemma="kingdom" pos="n1" xml:id="B04190-001-a-1190">Kingdom</w>
     </hi>
     <w lemma="of" pos="acp" xml:id="B04190-001-a-1200">of</w>
     <hi xml:id="B04190-e110">
      <w lemma="Ireland" pos="nn1" xml:id="B04190-001-a-1210">Ireland</w>
     </hi>
     <w lemma="seem" pos="vvz" xml:id="B04190-001-a-1220">seems</w>
     <pc xml:id="B04190-001-a-1230">,</pc>
     <w lemma="at" pos="acp" xml:id="B04190-001-a-1240">at</w>
     <w lemma="present" pos="j" xml:id="B04190-001-a-1250">present</w>
     <pc xml:id="B04190-001-a-1260">,</pc>
     <w lemma="to" pos="prt" xml:id="B04190-001-a-1270">to</w>
     <w lemma="be" pos="vvi" xml:id="B04190-001-a-1280">be</w>
     <w lemma="your" pos="po" xml:id="B04190-001-a-1290">your</w>
     <w lemma="master" pos="ng1" xml:id="B04190-001-a-1300">Master's</w>
     <w lemma="last" pos="ord" xml:id="B04190-001-a-1310">last</w>
     <hi xml:id="B04190-e120">
      <w lemma="stake" pos="n1" xml:id="B04190-001-a-1320">Stake</w>
      <pc xml:id="B04190-001-a-1330">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="B04190-001-a-1340">and</w>
     <w lemma="therefore" pos="av" xml:id="B04190-001-a-1350">therefore</w>
     <w lemma="must" pos="vmb" xml:id="B04190-001-a-1360">must</w>
     <w lemma="be" pos="vvi" xml:id="B04190-001-a-1370">be</w>
     <w lemma="manage" pos="vvn" xml:id="B04190-001-a-1380">managed</w>
     <w lemma="wise" pos="av-j" xml:id="B04190-001-a-1390">wisely</w>
     <pc xml:id="B04190-001-a-1400">;</pc>
     <w lemma="nor" pos="ccx" xml:id="B04190-001-a-1410">Nor</w>
     <w lemma="shall" pos="vmb" xml:id="B04190-001-a-1420">shall</w>
     <w lemma="you" pos="pn" xml:id="B04190-001-a-1430">you</w>
     <w lemma="want" pos="vvi" xml:id="B04190-001-a-1440">want</w>
     <w lemma="opportunity" pos="n1" xml:id="B04190-001-a-1450">Opportunity</w>
     <w lemma="of" pos="acp" xml:id="B04190-001-a-1460">of</w>
     <w lemma="make" pos="vvg" xml:id="B04190-001-a-1470">making</w>
     <w lemma="the" pos="d" xml:id="B04190-001-a-1480">the</w>
     <w lemma="most" pos="avs-d" xml:id="B04190-001-a-1490">most</w>
     <w lemma="of" pos="acp" xml:id="B04190-001-a-1500">of</w>
     <w lemma="your" pos="po" xml:id="B04190-001-a-1510">your</w>
     <w lemma="game" pos="n1" xml:id="B04190-001-a-1520">Game</w>
     <pc xml:id="B04190-001-a-1530">,</pc>
     <w lemma="since" pos="acp" xml:id="B04190-001-a-1540">since</w>
     <w lemma="we" pos="pns" xml:id="B04190-001-a-1550">we</w>
     <w lemma="be" pos="vvb" xml:id="B04190-001-a-1560">are</w>
     <w lemma="steadfast" pos="av-j" reg="steadfastly" xml:id="B04190-001-a-1570">stedfastly</w>
     <w lemma="resolve" pos="vvn" reg="resolved" xml:id="B04190-001-a-1580">resolv'd</w>
     <w lemma="to" pos="prt" xml:id="B04190-001-a-1590">to</w>
     <w lemma="give" pos="vvi" xml:id="B04190-001-a-1600">give</w>
     <w lemma="the" pos="d" xml:id="B04190-001-a-1610">the</w>
     <w lemma="army" pos="n1" xml:id="B04190-001-a-1620">Army</w>
     <w lemma="in" pos="acp" xml:id="B04190-001-a-1630">in</w>
     <hi xml:id="B04190-e130">
      <w lemma="England" pos="nn1" xml:id="B04190-001-a-1640">England</w>
     </hi>
     <w lemma="such" pos="d" xml:id="B04190-001-a-1650">such</w>
     <w lemma="powerful" pos="j" xml:id="B04190-001-a-1660">powerful</w>
     <w lemma="diversion" pos="n1" xml:id="B04190-001-a-1670">Diversion</w>
     <pc xml:id="B04190-001-a-1680">,</pc>
     <w lemma="that" pos="cs" xml:id="B04190-001-a-1690">that</w>
     <w lemma="we" pos="pns" xml:id="B04190-001-a-1700">we</w>
     <w lemma="doubt" pos="vvb" xml:id="B04190-001-a-1710">doubt</w>
     <w lemma="not" pos="xx" xml:id="B04190-001-a-1720">not</w>
     <w lemma="to" pos="prt" xml:id="B04190-001-a-1730">to</w>
     <w lemma="render" pos="vvi" xml:id="B04190-001-a-1740">render</w>
     <w lemma="they" pos="pno" xml:id="B04190-001-a-1750">them</w>
     <w lemma="whole" pos="av-j" xml:id="B04190-001-a-1760">wholly</w>
     <w lemma="incapable" pos="j" xml:id="B04190-001-a-1770">incapable</w>
     <w lemma="of" pos="acp" xml:id="B04190-001-a-1780">of</w>
     <w lemma="turn" pos="vvg" xml:id="B04190-001-a-1790">turning</w>
     <w lemma="their" pos="po" xml:id="B04190-001-a-1800">their</w>
     <w lemma="arm" pos="n2" xml:id="B04190-001-a-1810">Arms</w>
     <w lemma="towards" pos="acp" xml:id="B04190-001-a-1820">towards</w>
     <w lemma="you" pos="pn" xml:id="B04190-001-a-1830">You</w>
     <pc xml:id="B04190-001-a-1840">:</pc>
     <w lemma="in" pos="acp" xml:id="B04190-001-a-1850">In</w>
     <w lemma="the" pos="d" xml:id="B04190-001-a-1860">the</w>
     <w lemma="mean" pos="j" xml:id="B04190-001-a-1870">mean</w>
     <w lemma="time" pos="n1" xml:id="B04190-001-a-1880">time</w>
     <pc xml:id="B04190-001-a-1890">,</pc>
     <w lemma="we" pos="pns" xml:id="B04190-001-a-1900">we</w>
     <w lemma="advise" pos="vvb" xml:id="B04190-001-a-1910">advise</w>
     <w lemma="you" pos="pn" xml:id="B04190-001-a-1920">you</w>
     <w lemma="to" pos="prt" xml:id="B04190-001-a-1930">to</w>
     <w lemma="make" pos="vvi" xml:id="B04190-001-a-1940">make</w>
     <w lemma="all" pos="d" xml:id="B04190-001-a-1950">all</w>
     <w lemma="the" pos="d" xml:id="B04190-001-a-1960">the</w>
     <w lemma="levy" pos="n2" xml:id="B04190-001-a-1970">Levies</w>
     <w lemma="you" pos="pn" xml:id="B04190-001-a-1980">you</w>
     <w lemma="can" pos="vmb" xml:id="B04190-001-a-1990">can</w>
     <pc xml:id="B04190-001-a-2000">;</pc>
     <w lemma="and" pos="cc" xml:id="B04190-001-a-2010">and</w>
     <w lemma="by" pos="acp" xml:id="B04190-001-a-2020">by</w>
     <w lemma="no" pos="dx" xml:id="B04190-001-a-2030">no</w>
     <w lemma="mean" pos="n2" xml:id="B04190-001-a-2040">means</w>
     <w lemma="admit" pos="vvi" xml:id="B04190-001-a-2050">admit</w>
     <w lemma="any" pos="d" xml:id="B04190-001-a-2060">any</w>
     <hi xml:id="B04190-e140">
      <w lemma="heretical" pos="j" xml:id="B04190-001-a-2070">Heretical</w>
      <w lemma="villain" pos="n2" xml:id="B04190-001-a-2080">Villains</w>
     </hi>
     <w lemma="into" pos="acp" xml:id="B04190-001-a-2090">into</w>
     <w lemma="the" pos="d" xml:id="B04190-001-a-2100">the</w>
     <w lemma="least" pos="ds" xml:id="B04190-001-a-2110">least</w>
     <w lemma="command" pos="n1" xml:id="B04190-001-a-2120">Command</w>
     <pc xml:id="B04190-001-a-2130">,</pc>
     <hi xml:id="B04190-e150">
      <w lemma="civil" pos="j" xml:id="B04190-001-a-2140">Civil</w>
     </hi>
     <w lemma="or" pos="cc" xml:id="B04190-001-a-2150">or</w>
     <hi xml:id="B04190-e160">
      <w lemma="military" pos="j" xml:id="B04190-001-a-2160">Military</w>
      <pc xml:id="B04190-001-a-2170">:</pc>
     </hi>
     <w lemma="we" pos="pns" reg="We" xml:id="B04190-001-a-2180">VVe</w>
     <w lemma="have" pos="vvb" xml:id="B04190-001-a-2190">have</w>
     <w lemma="send" pos="vvn" xml:id="B04190-001-a-2200">sent</w>
     <w lemma="you" pos="pn" xml:id="B04190-001-a-2210">you</w>
     <w lemma="arm" pos="n2" xml:id="B04190-001-a-2220">Arms</w>
     <w lemma="for" pos="acp" xml:id="B04190-001-a-2230">for</w>
     <hi xml:id="B04190-e170">
      <w lemma="thirty" pos="crd" xml:id="B04190-001-a-2240">Thirty</w>
      <w lemma="thousand" pos="crd" xml:id="B04190-001-a-2250">Thousand</w>
      <pc xml:id="B04190-001-a-2260">;</pc>
     </hi>
     <w lemma="which" pos="crq" xml:id="B04190-001-a-2270">which</w>
     <pc xml:id="B04190-001-a-2280">,</pc>
     <w lemma="with" pos="acp" xml:id="B04190-001-a-2290">with</w>
     <w lemma="what" pos="crq" xml:id="B04190-001-a-2300">what</w>
     <w lemma="yourself" pos="pr" reg="yourself" xml:id="B04190-001-a-2310">your self</w>
     <w lemma="can" pos="vmb" xml:id="B04190-001-a-2330">can</w>
     <w lemma="supply" pos="vvi" xml:id="B04190-001-a-2340">supply</w>
     <pc xml:id="B04190-001-a-2350">,</pc>
     <w lemma="will" pos="n1" xml:id="B04190-001-a-2360">will</w>
     <w lemma="accouter" pos="ffr" xml:id="B04190-001-a-2370">Accouter</w>
     <w lemma="a" pos="d" xml:id="B04190-001-a-2380">a</w>
     <w lemma="formidable" pos="j" xml:id="B04190-001-a-2390">Formidable</w>
     <w lemma="body" pos="n1" xml:id="B04190-001-a-2400">Body</w>
     <w lemma="of" pos="acp" xml:id="B04190-001-a-2410">of</w>
     <w lemma="man" pos="n2" xml:id="B04190-001-a-2420">Men</w>
     <pc xml:id="B04190-001-a-2430">,</pc>
     <w lemma="and" pos="cc" xml:id="B04190-001-a-2440">and</w>
     <w lemma="full" pos="av-j" xml:id="B04190-001-a-2450">fully</w>
     <w lemma="sufficient" pos="j" xml:id="B04190-001-a-2460">sufficient</w>
     <w lemma="for" pos="acp" xml:id="B04190-001-a-2470">for</w>
     <w lemma="the" pos="d" xml:id="B04190-001-a-2480">the</w>
     <w lemma="entire" pos="j" xml:id="B04190-001-a-2490">entire</w>
     <w lemma="reduction" pos="n1" xml:id="B04190-001-a-2500">Reduction</w>
     <w lemma="of" pos="acp" xml:id="B04190-001-a-2510">of</w>
     <w lemma="that" pos="d" xml:id="B04190-001-a-2520">that</w>
     <hi xml:id="B04190-e180">
      <w lemma="kingdom" pos="n1" xml:id="B04190-001-a-2530">Kingdom</w>
      <pc unit="sentence" xml:id="B04190-001-a-2540">.</pc>
     </hi>
     <w lemma="we" pos="pns" reg="We" xml:id="B04190-001-a-2550">VVe</w>
     <w lemma="shall" pos="vmb" xml:id="B04190-001-a-2560">shall</w>
     <w lemma="also" pos="av" xml:id="B04190-001-a-2570">also</w>
     <w lemma="take" pos="vvi" xml:id="B04190-001-a-2580">take</w>
     <w lemma="care" pos="n1" xml:id="B04190-001-a-2590">care</w>
     <w lemma="to" pos="prt" xml:id="B04190-001-a-2600">to</w>
     <w lemma="furnish" pos="vvi" xml:id="B04190-001-a-2610">furnish</w>
     <w lemma="you" pos="pn" xml:id="B04190-001-a-2620">you</w>
     <w lemma="with" pos="acp" xml:id="B04190-001-a-2630">with</w>
     <w lemma="money" pos="n1" xml:id="B04190-001-a-2640">Money</w>
     <pc xml:id="B04190-001-a-2650">;</pc>
     <w lemma="and" pos="cc" xml:id="B04190-001-a-2660">and</w>
     <pc xml:id="B04190-001-a-2670">,</pc>
     <w lemma="in" pos="acp" xml:id="B04190-001-a-2680">in</w>
     <w lemma="the" pos="d" xml:id="B04190-001-a-2690">the</w>
     <w lemma="mean" pos="j" xml:id="B04190-001-a-2700">mean</w>
     <w lemma="time" pos="n1" xml:id="B04190-001-a-2710">time</w>
     <pc xml:id="B04190-001-a-2720">,</pc>
     <w lemma="we" pos="pns" xml:id="B04190-001-a-2730">we</w>
     <w lemma="advise" pos="vvb" xml:id="B04190-001-a-2740">advise</w>
     <w lemma="you" pos="pn" xml:id="B04190-001-a-2750">you</w>
     <w lemma="to" pos="prt" xml:id="B04190-001-a-2760">to</w>
     <w lemma="seize" pos="vvi" xml:id="B04190-001-a-2770">seize</w>
     <pc xml:id="B04190-001-a-2780">,</pc>
     <w lemma="without" pos="acp" xml:id="B04190-001-a-2790">without</w>
     <w lemma="distinction" pos="n1" xml:id="B04190-001-a-2800">distinction</w>
     <pc xml:id="B04190-001-a-2810">,</pc>
     <w lemma="all" pos="d" xml:id="B04190-001-a-2820">all</w>
     <w lemma="the" pos="d" xml:id="B04190-001-a-2830">the</w>
     <hi xml:id="B04190-e190">
      <w lemma="heretic" pos="n2" reg="Heretics" xml:id="B04190-001-a-2840">Hereticks</w>
     </hi>
     <w lemma="good" pos="n2-j" xml:id="B04190-001-a-2850">Goods</w>
     <w lemma="and" pos="cc" xml:id="B04190-001-a-2860">and</w>
     <w lemma="estate" pos="n2" xml:id="B04190-001-a-2870">Estates</w>
     <pc xml:id="B04190-001-a-2880">,</pc>
     <w lemma="which" pos="crq" xml:id="B04190-001-a-2890">which</w>
     <w lemma="will" pos="vmb" xml:id="B04190-001-a-2900">will</w>
     <w lemma="serve" pos="vvi" xml:id="B04190-001-a-2910">serve</w>
     <w lemma="for" pos="acp" xml:id="B04190-001-a-2920">for</w>
     <w lemma="a" pos="d" xml:id="B04190-001-a-2930">a</w>
     <w lemma="present" pos="j" xml:id="B04190-001-a-2940">present</w>
     <w lemma="advance" pos="n1" xml:id="B04190-001-a-2950">Advance</w>
     <pc xml:id="B04190-001-a-2960">:</pc>
     <w lemma="we" pos="pns" reg="We" xml:id="B04190-001-a-2970">VVe</w>
     <w lemma="will" pos="vmb" xml:id="B04190-001-a-2980">will</w>
     <w lemma="also" pos="av" xml:id="B04190-001-a-2990">also</w>
     <w lemma="supply" pos="vvi" xml:id="B04190-001-a-3000">supply</w>
     <w lemma="you" pos="pn" xml:id="B04190-001-a-3010">you</w>
     <w lemma="with" pos="acp" xml:id="B04190-001-a-3020">with</w>
     <w lemma="some" pos="d" xml:id="B04190-001-a-3030">some</w>
     <hi xml:id="B04190-e200">
      <w lemma="expert" pos="j" xml:id="B04190-001-a-3040">Expert</w>
      <w lemma="commander" pos="n2" xml:id="B04190-001-a-3050">Commanders</w>
      <pc xml:id="B04190-001-a-3060">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="B04190-001-a-3070">and</w>
     <w lemma="all" pos="d" xml:id="B04190-001-a-3080">all</w>
     <w lemma="necessary" pos="j" xml:id="B04190-001-a-3090">necessary</w>
     <hi xml:id="B04190-e210">
      <w lemma="engine" pos="n2" xml:id="B04190-001-a-3100">Engines</w>
      <w lemma="of" pos="acp" xml:id="B04190-001-a-3110">of</w>
      <w lemma="war" pos="n1" xml:id="B04190-001-a-3120">War</w>
      <pc xml:id="B04190-001-a-3130">;</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="B04190-001-a-3140">and</w>
     <w lemma="for" pos="acp" xml:id="B04190-001-a-3150">for</w>
     <w lemma="the" pos="d" xml:id="B04190-001-a-3160">the</w>
     <w lemma="supply" pos="n1" xml:id="B04190-001-a-3170">supply</w>
     <w lemma="of" pos="acp" xml:id="B04190-001-a-3180">of</w>
     <hi xml:id="B04190-e220">
      <w lemma="ammunition" pos="n1" xml:id="B04190-001-a-3190">Ammunition</w>
      <pc xml:id="B04190-001-a-3200">,</pc>
     </hi>
     <w lemma="we" pos="pns" xml:id="B04190-001-a-3210">we</w>
     <w lemma="be" pos="vvb" xml:id="B04190-001-a-3220">are</w>
     <w lemma="inform" pos="vvn" reg="Informed" xml:id="B04190-001-a-3230">Inform'd</w>
     <w lemma="of" pos="acp" xml:id="B04190-001-a-3240">of</w>
     <w lemma="your" pos="po" xml:id="B04190-001-a-3250">your</w>
     <w lemma="care" pos="n1" xml:id="B04190-001-a-3260">care</w>
     <pc xml:id="B04190-001-a-3270">,</pc>
     <w lemma="in" pos="acp" xml:id="B04190-001-a-3280">in</w>
     <w lemma="your" pos="po" xml:id="B04190-001-a-3290">your</w>
     <w lemma="speedy" pos="j" xml:id="B04190-001-a-3300">speedy</w>
     <w lemma="set" pos="vvg" xml:id="B04190-001-a-3310">setting</w>
     <w lemma="up" pos="acp" xml:id="B04190-001-a-3320">up</w>
     <hi xml:id="B04190-e230">
      <w lemma="mill" pos="n2" xml:id="B04190-001-a-3330">Mills</w>
     </hi>
     <w lemma="for" pos="acp" xml:id="B04190-001-a-3340">for</w>
     <w lemma="the" pos="d" xml:id="B04190-001-a-3350">the</w>
     <w lemma="make" pos="n1-vg" xml:id="B04190-001-a-3360">making</w>
     <w lemma="of" pos="acp" xml:id="B04190-001-a-3370">of</w>
     <hi xml:id="B04190-e240">
      <w lemma="powder" pos="n1" xml:id="B04190-001-a-3380">Powder</w>
      <pc xml:id="B04190-001-a-3390">,</pc>
      <w lemma="etc." pos="ab" reg="etc." xml:id="B04190-001-a-3400">&amp;c.</w>
      <pc unit="sentence" xml:id="B04190-001-a-3410"/>
     </hi>
     <w lemma="we" pos="pns" reg="We" xml:id="B04190-001-a-3420">VVe</w>
     <w lemma="likewise" pos="av" xml:id="B04190-001-a-3430">likewise</w>
     <w lemma="advise" pos="vvi" xml:id="B04190-001-a-3440">advise</w>
     <w lemma="you" pos="pn" xml:id="B04190-001-a-3450">you</w>
     <w lemma="to" pos="prt" xml:id="B04190-001-a-3460">to</w>
     <w lemma="keep" pos="vvi" xml:id="B04190-001-a-3470">keep</w>
     <hi xml:id="B04190-e250">
      <w lemma="strong" pos="j" xml:id="B04190-001-a-3480">strong</w>
     </hi>
     <w lemma="and" pos="cc" xml:id="B04190-001-a-3490">and</w>
     <hi xml:id="B04190-e260">
      <w lemma="diligent" pos="j" xml:id="B04190-001-a-3500">diligent</w>
      <w lemma="guard" pos="n2" xml:id="B04190-001-a-3510">Guards</w>
     </hi>
     <w lemma="in" pos="acp" xml:id="B04190-001-a-3520">in</w>
     <w lemma="all" pos="d" xml:id="B04190-001-a-3530">all</w>
     <w lemma="your" pos="po" xml:id="B04190-001-a-3540">your</w>
     <hi xml:id="B04190-e270">
      <w lemma="seaport" pos="n2" reg="Seaports" xml:id="B04190-001-a-3550">Sea-Ports</w>
     </hi>
     <w lemma="and" pos="cc" xml:id="B04190-001-a-3560">and</w>
     <hi xml:id="B04190-e280">
      <w lemma="coast" pos="n2" xml:id="B04190-001-a-3570">Coasts</w>
      <pc xml:id="B04190-001-a-3580">;</pc>
     </hi>
     <w lemma="to" pos="prt" xml:id="B04190-001-a-3590">to</w>
     <w lemma="set" pos="vvi" xml:id="B04190-001-a-3600">set</w>
     <w lemma="up" pos="acp" xml:id="B04190-001-a-3610">up</w>
     <hi xml:id="B04190-e290">
      <w lemma="beacon" pos="n2" xml:id="B04190-001-a-3620">Beacons</w>
     </hi>
     <w lemma="in" pos="acp" xml:id="B04190-001-a-3630">in</w>
     <w lemma="convenient" pos="j" xml:id="B04190-001-a-3640">convenient</w>
     <w lemma="place" pos="n2" xml:id="B04190-001-a-3650">places</w>
     <w lemma="throughout" pos="acp" xml:id="B04190-001-a-3660">throughout</w>
     <w lemma="the" pos="d" xml:id="B04190-001-a-3670">the</w>
     <hi xml:id="B04190-e300">
      <w lemma="kingdom" pos="n1" xml:id="B04190-001-a-3680">Kingdom</w>
      <pc xml:id="B04190-001-a-3690">,</pc>
     </hi>
     <w lemma="whereby" pos="crq" xml:id="B04190-001-a-3700">whereby</w>
     <w lemma="the" pos="d" xml:id="B04190-001-a-3710">the</w>
     <hi xml:id="B04190-e310">
      <w lemma="country" pos="n1" xml:id="B04190-001-a-3720">Country</w>
     </hi>
     <w lemma="may" pos="vmb" xml:id="B04190-001-a-3730">may</w>
     <w lemma="be" pos="vvi" xml:id="B04190-001-a-3740">be</w>
     <w lemma="ready" pos="j" xml:id="B04190-001-a-3750">ready</w>
     <w lemma="at" pos="acp" xml:id="B04190-001-a-3760">at</w>
     <w lemma="the" pos="d" xml:id="B04190-001-a-3770">the</w>
     <w lemma="least" pos="ds" xml:id="B04190-001-a-3780">least</w>
     <hi xml:id="B04190-e320">
      <w lemma="alarm" pos="n1" xml:id="B04190-001-a-3790">Alarm</w>
      <pc xml:id="B04190-001-a-3800">:</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="B04190-001-a-3810">And</w>
     <w lemma="as" pos="acp" xml:id="B04190-001-a-3820">as</w>
     <w lemma="we" pos="pns" xml:id="B04190-001-a-3830">we</w>
     <w lemma="doubt" pos="vvb" xml:id="B04190-001-a-3840">doubt</w>
     <w lemma="not" pos="xx" xml:id="B04190-001-a-3850">not</w>
     <w lemma="of" pos="acp" xml:id="B04190-001-a-3860">of</w>
     <w lemma="the" pos="d" xml:id="B04190-001-a-3870">the</w>
     <hi xml:id="B04190-e330">
      <w lemma="assistance" pos="n1" xml:id="B04190-001-a-3880">Assistance</w>
     </hi>
     <w lemma="of" pos="acp" xml:id="B04190-001-a-3890">of</w>
     <hi xml:id="B04190-e340">
      <w lemma="God" pos="nn1" xml:id="B04190-001-a-3900">God</w>
     </hi>
     <w lemma="in" pos="acp" xml:id="B04190-001-a-3910">in</w>
     <w lemma="so" pos="av" xml:id="B04190-001-a-3920">so</w>
     <w lemma="just" pos="j" xml:id="B04190-001-a-3930">Just</w>
     <w lemma="a" pos="d" xml:id="B04190-001-a-3940">a</w>
     <hi xml:id="B04190-e350">
      <w lemma="cause" pos="n1" xml:id="B04190-001-a-3950">Cause</w>
      <pc xml:id="B04190-001-a-3960">,</pc>
     </hi>
     <w lemma="so" pos="av" xml:id="B04190-001-a-3970">so</w>
     <w lemma="we" pos="pns" xml:id="B04190-001-a-3980">we</w>
     <w lemma="may" pos="vmb" xml:id="B04190-001-a-3990">may</w>
     <pc xml:id="B04190-001-a-4000">,</pc>
     <w lemma="with" pos="acp" xml:id="B04190-001-a-4010">with</w>
     <w lemma="all" pos="d" xml:id="B04190-001-a-4020">all</w>
     <w lemma="human" pos="j" xml:id="B04190-001-a-4030">human</w>
     <w lemma="probability" pos="n1" xml:id="B04190-001-a-4040">probability</w>
     <pc xml:id="B04190-001-a-4050">,</pc>
     <w lemma="conclude" pos="vvb" xml:id="B04190-001-a-4060">conclude</w>
     <w lemma="of" pos="acp" xml:id="B04190-001-a-4070">of</w>
     <w lemma="the" pos="d" xml:id="B04190-001-a-4080">the</w>
     <w lemma="speedy" pos="j" xml:id="B04190-001-a-4090">speedy</w>
     <w lemma="re-establishment" pos="n1" xml:id="B04190-001-a-4100">Re-establishment</w>
     <w lemma="of" pos="acp" xml:id="B04190-001-a-4110">of</w>
     <w lemma="our" pos="po" xml:id="B04190-001-a-4120">Our</w>
     <hi xml:id="B04190-e360">
      <w lemma="royal" pos="j" xml:id="B04190-001-a-4130">Royal</w>
      <w lemma="brother" pos="n1" xml:id="B04190-001-a-4140">Brother</w>
     </hi>
     <w lemma="in" pos="acp" xml:id="B04190-001-a-4150">in</w>
     <w lemma="his" pos="po" xml:id="B04190-001-a-4160">his</w>
     <hi xml:id="B04190-e370">
      <w lemma="throne" pos="n1" xml:id="B04190-001-a-4170">Throne</w>
      <w lemma="&amp;" pos="cc" xml:id="B04190-001-a-4180">&amp;</w>
      <w lemma="kingdom" pos="n1" xml:id="B04190-001-a-4190">Kingdom</w>
      <pc unit="sentence" xml:id="B04190-001-a-4200">.</pc>
     </hi>
    </p>
   </div>
  </body>
  <back xml:id="B04190-e380">
   <div type="colophon" xml:id="B04190-e390">
    <p xml:id="B04190-e400">
     <hi xml:id="B04190-e410">
      <w lemma="LONDON" pos="nn1" xml:id="B04190-001-a-4210">LONDON</w>
      <pc xml:id="B04190-001-a-4220">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="B04190-001-a-4230">Printed</w>
     <w lemma="for" pos="acp" xml:id="B04190-001-a-4240">for</w>
     <w lemma="t." pos="ab" xml:id="B04190-001-a-4250">T.</w>
     <w lemma="p." pos="ab" xml:id="B04190-001-a-4260">P.</w>
     <w lemma="1688." pos="crd" xml:id="B04190-001-a-4270">1688.</w>
     <pc unit="sentence" xml:id="B04190-001-a-4280"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
